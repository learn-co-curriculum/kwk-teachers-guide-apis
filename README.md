## Objectives

1. Students should have an basic understanding of what an Application Programming Interface is and its importance
2. Students should be able to successfully access remote APIs using JavaScript and/or jQuery

## Resources

* [Detailed Intro to APIs](https://zapier.com/learn/apis/)
* [jQuery AJAX Reference](https://www.w3schools.com/jquery/jquery_ref_ajax.asp)
* [AJAX Intro - W3 Schools](https://www.w3schools.com/xml/ajax_intro.asp)
* [AJAX - MDN](https://developer.mozilla.org/en-US/docs/Web/Guide/AJAX/Getting_Started)
